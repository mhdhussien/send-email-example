<?php

namespace App\Http\Controllers;

use App\Mail\SendMailable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function mail()
    {
        $name = 'test';
        Mail::to('test@gmail.com')->send(new SendMailable($name));
   
        return 'Email sent';
    }
}
